-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema estacionamento
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema estacionamento
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `estacionamento` DEFAULT CHARACTER SET utf8mb4 ;
USE `estacionamento` ;

-- -----------------------------------------------------
-- Table `estacionamento`.`automovel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `estacionamento`.`automovel` (
  `idAutomovel` INT(11) NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(45) NOT NULL,
  `tipo` INT(11) NOT NULL,
  `horaentrada` TIME NULL DEFAULT NULL,
  `horasaida` TIME NULL DEFAULT NULL,
  `idTemp` INT(11) NOT NULL,
  PRIMARY KEY (`idAutomovel`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- MySQL Workbench Forward Engineering

CREATE TABLE IF NOT EXISTS `estacionamento`.`operador` (
  `idOperador` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  
  PRIMARY KEY (`idOperador`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4;

INSERT INTO `estacionamento`.`operador` (`idOperador`, `login`, `senha`) VALUES ('1', 'pedro@gmail.com', 'qinetwork');