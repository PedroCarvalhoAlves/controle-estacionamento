var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');


var dbConfig = {
        host    : 'localhost',
        user    : 'root',
        //password: 'root',
        port    : '3306',
        database: 'estacionamento'
};


var test = mysql.createConnection(dbConfig);
test.connect(function(err){
  if(err){
    console.log('e: [DataBase] Not connected');
    return console.log(err);
  }
  console.log('i: [DataBase] Connected');
})
test.end();



app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



var port = process.env.PORT || 3000;
var estacionamento = express.Router();
// As duas variáveis abaixo são utilizadas para salvare o id na tabela carro ou
// na tabela moto de acordo com o tipo de automóvel selecionado no estacionamento
var tipoTemp = 0;
var idTemp = 0; 
var horaCarro = 8;
var horaMoto = 4;


estacionamento.post('/newAutomovelEntrada', function(req, res) {
    var placa = req.body.placa;
    var tipo = req.body.tipo;
    var horaentrada = req.body.horaentrada;

    var strQuery = "INSERT INTO automovel (placa, tipo, horaentrada " +
    ") VALUES ('" +placa + "','" + tipo + "','" + horaentrada +"');";

    
  var connection = mysql.createConnection(dbConfig);

connection.connect();
	connection.query(strQuery, function(err, rows, fields) {
		if (!err) {
			console.log('i: [QUERY] Insert successful - Table Automóvel');
			res.jsonp(rows);
        }
		else{
			console.log('i: [QUERY] Insert failed - Table Automóvel ERRO!');
			res.jsonp(err);
		}
	});
  
connection.end();



//var strQuery = "INSERT INTO temporario (idPrincipal,idTemp)+ VALUES('"+idTemp+"','"+idTemp+"');";

/*
connection2.connect();

connection2.query(strQuery, function(err, rows, fields) {
  if (!err) {
    console.log('i: [QUERY] Insert successful - Table Automóvel');
    res.jsonp(rows);
      }
  else{
    console.log('i: [QUERY] Insert failed - Table Automóvel ERRO!');
    res.jsonp(err);
  }
});
  connection2.end();
*/

});

estacionamento.post('/newAutomovelSaida', function(req, res) {
    var placa = req.body.placa;
    var horasaida = req.body.horasaida;
      
    var strQuery = "UPDATE automovel set horasaida ="+horasaida+
    " WHERE placa = '"+placa+"';";
    var connection = mysql.createConnection(dbConfig);


    
connection.connect();
	connection.query(strQuery, function(err, rows, fields) {
		if (!err) {
			console.log('i: [QUERY] Insert successful - Table Automóvel');
			res.jsonp(rows);
        }
		else{
			console.log('i: [QUERY] Insert failed - FALHOU NA INSERÇÃO AUTOMÓVEL!');
			res.jsonp(err);
		}
	});
  connection.end();
});

/*
As duas rotas abaixo foram comentadas porque o banco de dados inicialmente teria 3 tabelas. Devido ao tempo para
desenvolvimento da aplicação, foi definido que apenas uma tabela seria utilizada unificando todas as funções
do serviço de estacionamento.
*/

/*
estacionamento.post('/newCarro', function(req, res) {
  
  var strQuery = "INSERT INTO carro (idCarro, precoHora)" +
   " VALUES('SELECT max(idAutomovel) FROM automovel"+"', '"+horaCarro +"');";

  var connection = mysql.createConnection(dbConfig);

connection.connect();
connection.query(strQuery, function(err, rows, fields) {
  if (!err) {
    console.log('i: [QUERY] Insert successful - Table CARRO');
    res.jsonp(rows);
      }
  else{
    console.log('i: [QUERY] Insert failed - FALHOU NA INSERÇÃO CARRO!');
    res.jsonp(err);
  }
});
connection.end();
});
*/


/*
estacionamento.post('/newMoto', function(req, res) {
  
  var strQuery = "INSERT INTO moto (idMoto, precoHora" +
    ") VALUES ('" + " SELECT max(idTemp) FROM automovel)"+ "','" + horaMoto + "');";
  var connection = mysql.createConnection(dbConfig);

connection.connect();
connection.query(strQuery, function(err, rows, fields) {
  if (!err) {
    console.log('i: [QUERY] Insert successful - Table MOTO');
    res.jsonp(rows);
      }
  else{
    console.log('i: [QUERY] Insert failed - FALHOU NA INSERÇÃO MOTO!');
    res.jsonp(err);
  }
});
connection.end();
});

*/


estacionamento.post('/loginOperador', function(req, res) {
  var email = req.body.email;
  var senha = req.body.senha;
  console.log("ENTROU NA ROTA!");
  var strQuery = "SELECT * FROM operador WHERE login = '" + email + "' AND senha = '" + senha + "'";
  var connection = mysql.createConnection(dbConfig);

  connection.connect();
  connection.query(strQuery, function(err, rows, fields) {
    if (!err) {
      console.log('i: [QUERY] Login successful');
      res.jsonp(rows);
    }
    else {
      console.log('i: [QUERY] Login failed');
      res.jsonp(err);
    }
  });
  connection.end();
});









app.use('/estacionamento', estacionamento);

app.listen(port);
console.log('i: [APP] Starting application on port ' + port);